# -*- coding: utf-8 -*-
"""
Created on Thu May  6 16:15:22 2021

@author: aaron
"""

import pyb
from pyb import I2C
import utime


adcall = pyb.ADCAll(12, 0x70000)
adcall.read_core_vref()
i2c = I2C(1, I2C.MASTER)

#SLAVE ADDRESS: 0b0011000


def read_core():
    now = utime.ticks_ms()    
    while True:
        if utime.ticks_diff(utime.ticks_ms(), now)>= 1000:    
            now = utime.ticks_ms()
            temp = adcall.read_core_temp()
            print('Core temperature: '+ str(temp) + '[C]')
        yield(now)
        
        
def read_ambient():
    global Temperature
    Temperature = [0,0]
    now = utime.ticks_ms()    
    while True:
        if utime.ticks_diff(utime.ticks_ms(), now)>= 1000:    
            now = utime.ticks_ms()
            temp = i2c.mem_read(2, addr= 0b0011000, memaddr = 0b00000101) #0x18
            UpperByte = temp[0]
            LowerByte = temp[1]
            # if (UpperByte & 0x80) == 0x80:
            #     print('Cannot read this temperature value, temperature is higher than critical temperature')
            # elif (UpperByte & 0x60) == 0x60:
            #     print('Cannot read this temperature value, temperature is higher than upper temperature')
            # elif (UpperByte & 0x40) == 0x40:
            #     print('Cannot read this temperature value, temperature is lower than lower temperature')
            UpperByte = UpperByte & 0x1F
            if ((UpperByte & 0x10) == 0x10):
                UpperByte = UpperByte & 0x0F
                Temperature[0] = 256 - (UpperByte * 16 + LowerByte / 16)
                Temperature[1] = 'C'
            else:
                Temperature[0] = UpperByte * 16 + LowerByte / 16
                Temperature[1] = 'C'
            print('Ambient temperature: '+ str(Temperature) + '[C]')
        yield(now)
            
        
def check():
    addr = i2c.scan()
    if 24 in addr:
        print('The sensor is properly attached.')
    else:
        print('Something is wrong with the attachment, check sensor wiring.')        
    pass

def celsius():
    global Temperature
    if Temperature[1] == 'C':
        return(Temperature)
    elif Temperature[1] == 'F':
        Temperature[0] = (Temperature[0] - 32)*5/9
        Temperature[1] = 'F'
        return(Temperature)

def fahrenheit():
    global Temperature
    if Temperature[1] == 'C':
        Temperature[0] = Temperature[0] * 5/9 + 32
        Temperature[1] = 'C'
        return(Temperature)
    elif Temperature[1] == 'F':
        return(Temperature)
    pass
        
if __name__ == '__main__':
    __core__ = read_core()
    __amb__ = read_ambient()
    try:
        while True:
            next(__core__)
            next(__amb__)
    except KeyboardInterrupt:
        pass
            
        