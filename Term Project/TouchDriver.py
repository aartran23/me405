# -*- coding: utf-8 -*-
"""
Created on Sun May 16 14:59:25 2021

@author: aaron
"""
import pyb

class TouchDriver:
    '''
    Reads position of objects in x and y on resistive touch panel
    '''
    def __init__(self,P1, P2, P3, P4):
        ##@brief CPU pin connected to ym
        self.ym = pyb.Pin(P1, mode = pyb.Pin.OUT_PP, value = 0)
        ##@brief CPU pin connected to xm
        self.xm = pyb.Pin(P2, mode = pyb.Pin.OUT_PP, value = 0)
        ##@brief CPU pin connected to yp
        self.yp = pyb.Pin(P3, mode = pyb.Pin.OUT_PP, value = 0)
        ##@brief CPU pin connected to xp
        self.xp = pyb.Pin(P4, mode = pyb.Pin.OUT_PP, value = 0)
        ##@brief configure xm fpr ADC
        self.xADC = pyb.ADC(self.xm)
        ##@brief configure ym for ADC
        self.yADC = pyb.ADC(self.ym)
        ##@brief calibrated x origin position relative to bottom left corner
        self.xc = 176/2
        ##@brief calibrated y origin position relative to bottom left corner
        self.yc = 100/2
        
    
    def x_scan(self):
        '''
        Reads x position
        '''
        self.xp.high()
        self.yp.low()
        
        return(self.xADC.read())
    
    def y_scan(self):
        '''
        Reads y position
        '''
        self.xp.low()
        self.yp.high()
        
        return(self.yADC.read())
    
    def z_scan(self):
        '''
        Sense if anything is in contact with touch panel
        '''
        self.xp.low()
        self.yp.high()
        
        if self.xADC.read() == 0:
            return False
        elif self.xADC.read() != 0:
            print(self.xADC.read())
            return True
        else:
            print('Error in z scan')
    
    def read(self):
        '''
        Reads x, y, and z
        '''
        return(self.x_scan(), self.y_scan(), self.z_scan())

        
if __name__ == '__main__':    
    ##@brief selected UART port
    myuart = pyb.UART(2)
    pyb.repl_uart(None)
    PA0 = pyb.Pin.cpu.A0
    PA1 = pyb.Pin.cpu.A1
    PA6 = pyb.Pin.cpu.A6
    PA7 = pyb.Pin.cpu.A7
    TouchD = TouchDriver(PA0,PA1,PA6,PA7)