# -*- coding: utf-8 -*-
"""
Created on Wed Apr 28 00:45:21 2021

@author: aaron
"""
import pyb
import micropython
import utime

micropython.alloc_emergency_exception_buf(200)

then = utime.ticks_ms()
delt = 0
last_compare = 0
flag = False
cap = 0
state = 2
rxn = 0.1

def OC_callback(TIM_SRC):
    global last_compare
    global then
    global delt
    global first
    last_compare = t2ch1.compare()
    last_compare += 10000
    if last_compare > 0xFFFF:
        last_compare = last_compare - 0xFFFF - 1
    t2ch1.compare(last_compare)
    delt = utime.ticks_diff(utime.ticks_ms(), then)
    print('lc:')
    print(last_compare)
    print('yeet')
    print(delt)


def IC_callback(TIM_SRC):
    global cap
    global last_compare
    global rxn
    global state
    #global flag
    if state == 2:
        #flag = True
        cap = t2ch1.capture()

        print(rxn)


tim2 = pyb.Timer(2, prescaler = 8000, period= 0xFFFF)
LED = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
pinB3 = pyb.Pin(pyb.Pin.cpu.B3, pyb.Pin.IN)
t2ch2 = tim2.channel(2, mode = pyb.Timer.IC, callback = IC_callback, pin = pinB3)


t2ch1 = tim2.channel(1, pyb.Timer.OC_TOGGLE, pin= LED, callback = OC_callback)

while True:
    try:
        rxn = (cap - last_compare)*8000/80000
        print(rxn)
        pass
    except KeyboardInterrupt:
        print('noce')
