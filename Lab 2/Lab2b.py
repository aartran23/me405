'''@file        Lab2b.py
@brief          Measures reaction time
@details        A delay is randomly chosen upon initialization after which an LED will turn on. The user will react to the LED turning on by pressing the user button. 
                Reaction times are recorded and stored. The program will loop again after the user has reacted. 
                The average reaction time, as well as a list of the reaction times will be presented upon exiting the program wiht Ctrl + C.
                Unlike Lab2a.py which uses an external interrupt, this code makes use of output compare and input capture to achieve the same result.
                Source code can be found at https://bitbucket.org/aartran23/me405/src/master/Lab%202/Lab2b.py
                A demonstration of the functionality can be found at https://cpslo-my.sharepoint.com/:v:/g/personal/atran189_calpoly_edu/ESsvFUixwlhIjxNwNql_N2oBcC59Evv_Qp4vOXl87JGF7w?e=8jeLzw
                Additionally, to use this code, a jumper wire had to be connected to pins A5 and C13. An image of this can be found in Figure 2.
@image          html Lab2STD.png "Figure 1: State Transition Diagram for Lab 2"
@image          html Lab2b_SETUP.jpg "Figure 2: Nucleo Configuration to run Lab2b.py"
@author         Aaron Tran
@date           4/29/21
'''

import utime
import pyb
import random
import micropython


micropython.alloc_emergency_exception_buf(200)

def OC_callback(TIM_SRC):
    '''@brief Interrupt service routine on output compare
    @details handles turning on and off the LED on pin A5, will retrigger to turn LED off after 1 second
    @param TIM_SRC timer source input, passed in by timer 2 channel 1 on output compare
    '''
    global last_compare
    global state
    global flag
    global delay_ct
    if state == 0:
        # Don't do anything if FSM is in state 0
        pass
    elif state == 1:
        # If called in state 1, turn LED on, prepare output compare to retrigger after 1 second, and go to state 2
        pinA5.high()
        last_compare = delay_ct
        next_compare = last_compare + 10000
        if next_compare > 0xFFFF:
            next_compare = next_compare - 0xFFFF - 1
        t2ch1.compare(next_compare)
        state =2
    elif state == 2:
        # If called in state 2, which should only happen if 1 second has passed, turn LED off
        pinA5.low()


    
    
def IC_callback(TIM_SRC):
    '''@brief Interrupt service routine on input capture
    @details capture timer count when pin B3 has a falling edge
    @param TIM_SRC timer source input, passed in by timer 2 channel 2 on input capture
    '''
    global cap
    global last_compare
    global rxn
    global state
    global flag
    if state == 2:
        flag = True
        cap = t2ch2.capture()

##@brief last compare value set for Output Compare
last_compare = 0
##@brief last capture value from input capture
cap = 0
##@brief reaction time recorded
rxn = 0

##@brief flag to tell FSM that user has reacted
flag = False

##@brief pin B3 object configured for input
pinB3 = pyb.Pin(pyb.Pin.cpu.B3, pyb.Pin.IN)
##@brief pin A5 object configured for output
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
##@brief Timer 2 configured for a prescaler of 8000 and a period of OxFFFF
tim2 = pyb.Timer(2, prescaler = 8000, period = 0xFFFF)
##@brief Timer 2 Channel 2 configured for output compare
t2ch1 = tim2.channel(1, mode = pyb.Timer.OC_TOGGLE, callback = OC_callback)
##@brief Timer 2 Channel 1 configured for input capture on pin B3
t2ch2 = tim2.channel(2, mode = pyb.Timer.IC, callback = IC_callback, pin = pinB3)


def rxn_task():
    '''@brief Finite State Machine of Lab 2b
    @details Runs through states 0 to 2 as described in the state transition diagram, responsible for delaying, playing led, and captureing reaction time
    '''
    global state
    global avg_speed
    global flag
    global speeds
    global delay_ct
    ##@brief state of finite state machine
    state = 0
    ##@brief number of times finite state machine has looped
    runs = 0
    ##@brief list of reaction times
    speeds = []
    while True:
        if state == 0:
            # State 0 INIT, generates delay
            print('Get Ready!')
            runs+=1
            ##@brief delay converted to ticks
            delay = random.randint(2000,3000)/1000*80000000/8000
            ##@brief delay converted to output compare value
            delay_ct = round(tim2.counter()+ delay)
            if delay_ct >= 0xFFFF:
                delay_ct -= 0xFFFF
            print('Delaying for ' + str(delay_ct) + ' counts')
            # start = utime.ticks_ms()
            state = 1
        elif state == 1:
            # State 1 WAIT, will turn on LED and start time after delay, feeds output compare value to timer 2 channel 1
            t2ch1.compare(delay_ct)
            flag = False
        elif state == 2:
             # State 2 REACT, will turn LED off and record reaction time after user input
            print(tim2.counter())
            if flag == True:
                pinA5.low()
                print('capture:' + str(cap))
                print('lc:' + str(last_compare))
                if cap>=last_compare:
                    rxn = (cap - last_compare)*8000/80000
                elif cap<last_compare:
                    rxn =( 0xFFFF-last_compare + cap)*8000/80000
                print('Your reaction time is ' + str(rxn) + ' microseconds.')
                speeds.append(rxn)
                ##@brief average reaction times
                avg_speed = sum(speeds)/runs
                state = 0
        yield(state)



if __name__ == "__main__":
    __react__ = rxn_task()
    __then__ = utime.ticks_us() 
    try: 
        while True:
            if utime.ticks_diff(utime.ticks_us(),__then__)>=10:   
                next(__react__)
                __then__ = utime.ticks_us()
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
        print('Your average reaction time was '+ str(avg_speed)+ ' microseconds.')
        print(speeds)
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')
    pass