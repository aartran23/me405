'''@file        Lab2a.py
@brief          Measures reaction time
@details        A delay is randomly chosen upon initialization after which an LED will turn on. The user will react to the LED turning on by pressing the user button. 
                Reaction times are recorded and stored. The program will loop again after the user has reacted. 
                The average reaction time, as well as a list of the reaction times will be presented upon exiting the program wiht Ctrl + C.
                Source code can be found at https://bitbucket.org/aartran23/me405/src/master/Lab%202/Lab2a.py
                A demonstration of the functionality can be found at https://cpslo-my.sharepoint.com/:v:/g/personal/atran189_calpoly_edu/EXLLhbwfqDFGlBti_UDSjasBGIACeNZoKqpzBQhtRHpAOQ
@image          html Lab2STD.png "State Transition Diagram for Lab 2"
@author         Aaron Tran
@date           4/29/21
'''



"""
Created on Fri Apr 23 12:37:56 2021
@file Lab2a.py
@author: aaron
https://cpslo-my.sharepoint.com/:v:/g/personal/atran189_calpoly_edu/EXLLhbwfqDFGlBti_UDSjasBGIACeNZoKqpzBQhtRHpAOQ?e=Fsqohe
"""

import utime
import pyb
import random

def Button(IRQ_src):
    '''@brief callback function
    @param IRQ_src when triggered in state 2, will trigger flag to let FSM know user has reacted
    '''
    global react_T
    global state
    global flag
    ##@brief reaction timestamp
    react_T = utime.ticks_us()
    if state == 2:
        ##@brief reaction flag
        flag = True
        print('flagged')


##@brief pin C13 object 
pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
##@brief configure pin C13 for callback on falling edges
interrupt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback= Button)
##@brief pin A5 object
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)


def rxn_task():
    '''@brief Finite State Machine of Lab 2a
    @details Runs through states 0 to 2 as described in the state transition diagram, responsible for delaying, playing led, and captureing reaction time
    '''
    global state
    global avg_speed
    global flag
    global speeds
    ##@brief FSM state
    state = 0
    ##@brief number of times FSM has been ran
    runs = 0
    ##@brief list containing collected reaction times
    speeds = []
    while True:
        if state == 0:
            # State 0 INIT, generates delay
            runs+=1
            ##@brief delay randomly chosen before LED goes off
            delay = random.randint(2000,3000)
            __start__ = utime.ticks_ms()
            state = 1
        elif state == 1:
            # State 1 WAIT, will turn on LED and start time after delay 
            if utime.ticks_diff(utime.ticks_ms(),__start__) >= delay:
                ##@brief time when LED turns on
                react_now = utime.ticks_us()
                pinA5.high()
                state = 2
                flag = False
                print('Now!')
        elif state == 2:
            # State 2 REACT, will turn LED off and record reaction time after user input
            if flag == True:
                pinA5.low()
                ##@brief recorded reaction time
                rxn_time = utime.ticks_diff(react_T,react_now)
                print('Your reaction time is ' + str(rxn_time) + ' microseconds.')
                speeds.append(rxn_time)
                ##@brief 
                avg_speed = sum(speeds)/runs
                state = 0
        yield(state)



if __name__ == "__main__":
    __react__ = rxn_task()
    __then__ = utime.ticks_us() 
    try: 
        while True:
            if utime.ticks_diff(utime.ticks_us(),__then__)>=100:   
                next(__react__)
                __then__ = utime.ticks_us()
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
        print('Your average reaction time was '+ str(avg_speed)+ ' microseconds.')
        print(speeds)
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')
    pass