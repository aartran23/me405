# -*- coding: utf-8 -*-
'''@file        getChange.py
@brief          Contains function for determining change due
@details        Contains function for determining change due, given price and payment. Output is in form of dictionary.
@author         Aaron Tran
@date           4/2/21
'''

def getChange(price,payment):
    coin_dict = {"5 Dollar Bills":500, "1 Dollar Bills": 100, "Quarters": 25, "Dimes": 10, "Nickels": 5, "Pennies": 1 }
    ##coin_dict = {"Pennies": 0.01, "Nickels": 0.05, "Dimes":0.10, "Quarters":0.25, "1 Dollar Bill":1.00, "5 Dollar Bill":5.00, "10 Dollar Bill":10.00, "20 Dollar Bill":20.00, "50 Dollar Bill":50.00, "100 Dollar Bill":100.00}
    coins = [500, 100, 25, 10, 5, 1]
    coin_ct = []
    if price > payment:
        change_due = 0
        print('You need more money')
    elif price <= payment:    
        change_due = 100*payment - 100*price
    for coin in coins:
        if change_due == 0:
            break
        if change_due%coin == change_due:
            coin_ct.append(0)
        else:
            coin_ct.append((change_due - change_due%coin)/coin)
            change_due -= coin*coin_ct[-1]
    n=0
    text = ''
    print(coin_ct)
    for val in coin_dict: 
        coin_dict[val] = coin_ct[n]
        text += (val + ': ' + str(coin_ct[n]) + '\n') 
        n+=1

    print(text)
    print(coin_dict)
    return coin_dict
        
        
    
if __name__ == "__main__":
    trash = getChange(0, 0.50)
    pass

