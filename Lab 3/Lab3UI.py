'''@file        Lab3UI.py
@brief          interprets Nucleo data, plots it and generates .CSV file
@details        This is the file intended to be ran on the computer. It receives serial data from Lab3main.py. This code implements a finite state machine, whose state transition diagram can be found in Figure 1.
                Data collection for the PC can be conducted using this script. This script will generate a .CSV file containing relevant data, as well as plots of the data. 
                The .CSV file will be named lab3data.csv, with some numerical suffix representing the version of data created. Source code can be found at https://bitbucket.org/aartran23/me405/src/master/Lab%203/Lab3UI.py
@image          html Lab3main_STD.png "Figure 1. State Transition Diagram of Lab3UI.py"
@author         Aaron Tran
@date           5/1/21
'''
import serial
import keyboard
import csv
from time import sleep
from matplotlib import pyplot

def kb_cb(key):
    """@brief Callback function which is called when a key has been pressed.
    @param key interrupt callback source
    """
    global last_key
    ##@brief last key pressed
    last_key = key.name
    if last_key != 'n':
        ser.write(str(last_key).encode('ascii'))

def make_csv(file,x1,y1):
    '''@brief writes data to csv file
    @param file existing file to be used to write the CSV data into
    @param x1 x values of first data set
    @param y1 y values of first data set
    '''
    __n__ = 0
    with open(file, mode = 'w',newline='') as data:
        __data__ = csv.writer(data, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        __data__.writerow(['Time [s]', 'ADC counts [-]'])
        for point in x1:
            __data__.writerow([str(x1[__n__]), str(y1[__n__])])
            __n__+=1
    pass


def run_UI():
    '''@brief Runs as a generator to implement the Finite State Machine for Lab3main.py contained within
    '''
    global last_key
    global state
    global first_run
    global ver
    global volts
    global times
    
    while True:
        ##@brief string received from Nucleo
        myLineString = ser.readline().decode()
        ##@brief myLineString stripped of extraneous string data like \n and \r
        myStrippedString = myLineString.strip()
        ##@brief list containing data in each CSV
        mySplitStrings = myStrippedString.split(',')    
        if mySplitStrings[0] == 'UI':
            #checks prefix and lets user know message that Nucleo sends
            print(mySplitStrings[1])
            last_key = None     
        if state == 0:
            # initializes code if user presses g
            if first_run:
                ##@brief dictates if this is the first run of state 0 in the FSM iteration
                first_run = False
                ##@brief list of time data
                times = []
                for n in range(0,5000):
                    times.append(n/100000)
                ##@brief list of capcitor voltage data
                volts = []
                ##@brief list of ADC counts
                counts = []
                print('Press g to begin taking data')     
            if last_key == 'g':
                ##@brief user input, can only be g
                last_key = None
                ##@brief state of FSM
                state = 1
                print('state 2')
                first_run = True
        elif state == 1:
            try:
                #checks if prefix of serial message is 'data' or 'done' and acts accordingly
                if mySplitStrings[0] == 'Done':
                    print('state 3')
                    state = 2
                elif mySplitStrings[0] == 'data':
                    __count__ = float(mySplitStrings[1])
                    #convert ADC count to voltage
                    __volt__ =  __count__ * 3.3/0b111111111111
                    print(__volt__)
                    volts.append(__volt__)
                    counts.append(__count__)
            except:
                pass
        elif state == 2:
            #writes data to CSV and plots it
            try: 
                ##@brief .CSV filename 
                filename = 'lab3data('+str(ver)+').csv'
                ##@brief generates .CSV file                      
                f = open(filename, "x")
                make_csv(filename,times, counts)
                state = 0
                pyplot.figure()
                pyplot.plot(times, volts, 'k:')
                pyplot.xlabel('Time [s]')
                pyplot.ylabel('Capacitor Voltage [V]')
                pyplot.ylim([0,3.3])
                pyplot.show()           
            except:
                ##@brief version number of data
                ver+=1
            
##@brief handle for serial port
ser = serial.Serial(port='COM5', baudrate=115273, timeout=1)
__keys__ = keyboard.on_release_key("G", callback=kb_cb)
##@brief state of Finite State Machine
state = 0
##@brief dictates if this is the first run of state 0 in the FSM iteration
first_run = True
##@brief version number of data
ver = 0
##@brief last key pressed, can only be 'g'
last_key = None

if __name__ == "__main__":
    try:
        run_UI()
    except KeyboardInterrupt:
        ser.close()
    