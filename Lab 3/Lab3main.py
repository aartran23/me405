'''@file        Lab3main.py
@brief          Collects ADC count data on capacitor C15
@details        This file is intended to be imported into the Nucleo as "main.py" and run cooperatively with Lab3UI.py. 
                To collect good data, the user must hold down the user button before data collection and release it just a couple milliseconds
                after pressing 'g.' Source code can be found at https://bitbucket.org/aartran23/me405/src/master/Lab%203/Lab3main.py
@image          html Lab3main_STD.png "Figure 1. State Transition Diagram of Lab3main.py"
@author         Aaron Tran
@date           5/1/21
'''

import pyb
from pyb import UART
from array import array
import utime
import micropython


micropython.alloc_emergency_exception_buf(200)
##@brief pin A0 object
pinA0 = pyb.Pin.cpu.A0
##@brief configure pin A0 to an analog to digital converter(ADC) 
ADC = pyb.ADC(pinA0)
##@brief timer 2 object
tim2 = pyb.Timer(2, freq = 100000)


def run_task():
    '''@brief Runs as a generator to implement the Finite State Machine for Lab3main.py contained within
    '''
    ##@brief csv index
    n = 0
    ##@brief Current State
    state = 0
    ##@brief handle for UART port   
    myuart = UART(2)
    ##@brief ASCII encoded data input        
    val = None
    while True:
        #detect user input and store value in val
        if myuart.any() != 0:
                val = myuart.readchar()
                print('You pressed ASCII' + str(val))
        if state == 0:
                # initialize program if user hits "g" and go to state 1
                if val == 103:
                    print('Ready to take data')
                    myuart.write('{:}, {:}\r\n'.format('UI', 'Ready to take data!'))
                    val = None
                    volts = array('f', 5000*[0])  
                    state = 1
        elif state == 1:
            # store the ADC counts in volts using timer 2 for the timing
            ADC.read_timed(volts, tim2)
            state = 2
        elif state == 2:
            #send data to computer
            for n in range(len(volts)): 
                myuart.write('{:}, {:}\r\n'.format('data', volts[n]))
            print('Done')
            state = 0
            myuart.write('Done,Done\r\n')
            
        yield(state)

if __name__ == "__main__":
    __daq__ = run_task() 
    try: 
        while True:
            next(__daq__)
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')
    pass