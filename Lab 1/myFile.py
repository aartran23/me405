"""@file myFile.py 
@brief Simulates usage of a vending machine
@details Users are able to interact with a virtual vending machine by the name of Vendotron. Users may select from any of four drinks and are able to insert any number of monetary denominations ranging from the penny to a five dollar bill.
         Users are also able to eject change at any point. See the state transistion diagram in Figure 1 below. Source code can be found at https://bitbucket.org/aartran23/me405/src/master/Lab%201/myFile.py
@image html Lab1_STD.png "Figure 1. State Transition Diagram of Vendotron Finite State Machine"
@author Aaron Tran
@date April 18, 2021
"""

import keyboard
from time import sleep

def kb_cb(key):
    """@brief Callback function which is called when a key has been pressed.
    @param key interrupt callback source
    """
    global last_key
    ##@brief last key pressed
    last_key = key.name
    
        
def getChange(price,payment):
    """@brief Calculates leftover change due
    @param price balance due
    @param payment amount paid
    """
    ##@brief Dictionary relating denominations to monetary value
    coin_dict = {"5 Dollar Bills":500, "1 Dollar Bills": 100, "Quarters": 25, "Dimes": 10, "Nickels": 5, "Pennies": 1 }
    ##@brief List containing possible denominations
    coins = [500, 100, 25, 10, 5, 1]
    ##@brief list containing number of each denomination due
    coin_ct = []
    if price > payment:
        change_due = 0
        print('You need more money')
    elif price <= payment:    
        change_due = 100*payment - 100*price
    for coin in coins:  
        if change_due%coin == change_due:
            coin_ct.append(0)
        else:
            coin_ct.append((change_due - change_due%coin)/coin)
            change_due -= coin*coin_ct[-1]
    n=0
    __text__ = ''
    for val in coin_dict: 
        coin_dict[val] = coin_ct[n]
        __text__ += (val + ': ' + str(coin_ct[n]) + '\n') 
        n+=1

    print('Change dispensed as follows: \n' + __text__)
    return coin_dict
        

def VendotronTask():
    """ Runs one iteration of the task
    """
    global last_key
    ##@brief Dictionary of entry value to drink
    drinks = {'c':'Cuke', 'p':'Popsy', 's':'Spryte', 'd':'Dr.Pupper'}
    ##@brief Dictionary of entry value to coin input
    coin_input = {'0':0.01,'1':0.05, '2':0.10, '3':0.25, '4':1.00, '5':5.00 }
    ##@brief Prices of drinks
    drink_cost = {'Cuke':1.50,'Popsy':1.75,'Spryte':2.50,'Dr.Pupper':20.00}  
    keyboard.on_release_key("0", callback=kb_cb)
    keyboard.on_release_key("1", callback=kb_cb)
    keyboard.on_release_key("2", callback=kb_cb)
    keyboard.on_release_key("3", callback=kb_cb)
    keyboard.on_release_key("4", callback=kb_cb)
    keyboard.on_release_key("5", callback=kb_cb)
    keyboard.on_release_key("c", callback=kb_cb)
    keyboard.on_release_key("p", callback=kb_cb)
    keyboard.on_release_key("s", callback=kb_cb)
    keyboard.on_release_key("d", callback=kb_cb)
    keyboard.on_release_key("e", callback=kb_cb)
    keyboard.on_release_key("y", callback=kb_cb)
    keyboard.on_release_key("n", callback=kb_cb)
    ##@brief balance available to user
    balance = 0
    last_key = None
    ##@brief state of machine
    state = 0
    ##@brief timer of system
    time = 0
    
    while True:
        # Implement FSM using a while loop and an if statement
        # will run eternally until user presses CTRL-C
        time+=0.1
        if time >= 10:
            print('Try Cuke today!')
            time = 0
        if last_key in coin_input:
            balance += coin_input[last_key]
            balance = round(balance, 2)
            last_key = None
            print('Balance: $' + str(balance))      
        if state == 0:
            # perform state 0, "INIT" operations
            # this init state, initializes the FSM itself, with all the
            # code features already set up
            print('Greetings! This is the Vendotron! To select Cuke, Popsy, Spryte, or Dr.Pupper, enter c, p, s, or d respectively. If you would like to eject your change, press E.')
            print('Select from the following:\n Cuke -- $1.50\n Popsy -- $1.75\n Spryte -- $2.50\n Dr.Pupper -- $20.00*\n\n *To discourage purchase of this horrible drink, we have inflated the cost significantly')
            print('Payment options:\n Pennies, press 0\n Nickels, press 1\n Dimes, press 2\n Quarters, press 3\n 1 Dollar Bills, press 4\n 5 Dollar Bills, press 5')
            state = 1       # on the next iteration, the FSM will run state 1
                 
        elif state == 1:
            # perform state 1, "MAIN_UI" operations
            if last_key in drinks:    
                drink = drinks[last_key]
                cost = drink_cost[drink]
                last_key = None
                if cost<= balance:    
                    state = 2
                    balance -= cost
                    print('Please take your ' + drink + '!')
                    print('Remaining balance: $' + str(balance))
                    print('Would you like to select another drink? Y/N')
                elif cost>balance:
                    print('Insufficient Funds')
                    print('You have $'+ str(balance) + ', but '+ drink +' costs $' + str(cost))
            elif last_key == 'e':
                last_key = None
                state = 3
                        
        elif state == 2:
            # perform state 2, "CONT/END" operations
            if last_key == 'y':
                state = 1
                last_key = None
                print('Please select another beverage.')
            elif last_key == 'n':
                state = 3
        
        elif state == 3:
            print('state 3')
            getChange(0, balance)
            balance = 0
            state = 0

        else:
            # this state shouldn't exist!
            
            pass
        
        yield(state)



if __name__ == "__main__":
    # Initialize code to run FSM (not the init state). This means building the 
    # iterator as generator object
    __vendo__ = VendotronTask()
    
    # Run the task every 100ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    try:
        while True:
            next(__vendo__)
            sleep(0.1)
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')